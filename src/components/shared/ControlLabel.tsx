import React from "react";

interface IControlLabelProps {
	text: string;
}

export const ControlLabel: React.FC<IControlLabelProps> = ({ text }) => {
	return <h4 className="control-label">{text}</h4>;
};
