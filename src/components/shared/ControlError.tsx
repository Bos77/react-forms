import React from "react";
import cn from "classnames";

interface IControlErrorProps {
	error?: boolean;
	errorMessage?: string;
}

export const ControlError: React.FC<IControlErrorProps> = ({ error, errorMessage }) => {
	return (
		<span className={cn("control-error", { "control-error_visible": error })}>
			{errorMessage}
		</span>
	);
};
