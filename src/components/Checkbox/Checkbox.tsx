import React, { useState, useEffect, ChangeEvent } from "react";
import cn from "classnames";

import "./Checkbox.scss";

interface ICheckboxProps {
	className: string;
	label: string;
	isChecked: boolean;
	onValueChange: Function;
}

export const Checkbox: React.FC<ICheckboxProps> = ({
	className = "",
	label = "",
	isChecked = false,
	onValueChange,

	...inputProps
}) => {
	const classNames = cn("checkbox", "cursor_pointer", "text-select_none", className);
	const [checked, setChecked] = useState(isChecked);

	const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
		const newVal = event.target.checked;
		setChecked(newVal);
		onValueChange && onValueChange(newVal);
	};

	useEffect(() => {
		setChecked(isChecked);
	}, [isChecked]);

	return (
		<label className={classNames}>
			<div className="checkbox__label">
				<input
					type="checkbox"
					hidden
					checked={checked}
					onChange={handleChange}
					{...inputProps}
				/>
				<span className="checkbox__checkmark">
					<span className="checkbox__ticket"></span>
				</span>
			</div>
		</label>
	);
};
