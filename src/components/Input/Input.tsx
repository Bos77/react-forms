import React from "react";
import cn from "classnames";

import { ControlError } from "../shared/ControlError";
import { ControlLabel } from "../shared/ControlLabel";

import "./Input.scss";

interface IInputProps {
	label: string;
	inputType: "text" | "password" | "date" | "number";
	wrapperClassName?: string;
}

export const Input: React.FC<IInputProps> = ({ label, inputType, wrapperClassName = "" }) => {
	return (
		<div className={cn("input", "control", wrapperClassName)}>
			<ControlLabel text={label} />
			<input type={inputType} className="input__field" />
			<ControlError />
		</div>
	);
};
