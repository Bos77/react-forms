import React, { MouseEventHandler } from "react";
import cn from "classnames";

import "./Button.scss";

interface IButtonProps {
	htmlType: "button" | "reset" | "submit";
	text: string;
	onClick?: MouseEventHandler;
	variant: "primary" | "outlined";
	className: string;
	isDisabled?: Boolean;
}

export const Button: React.FC<IButtonProps> = ({
	htmlType,
	text,
	onClick,
	isDisabled = false,
	variant,
	className,
}) => {
	return (
		<button
			type={htmlType}
			className={cn(`btn_${variant}`, className, { btn_disabled: isDisabled })}
			onClick={onClick}
			disabled={!!isDisabled}
		>
			{text}
		</button>
	);
};
