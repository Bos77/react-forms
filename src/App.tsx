import React from "react";

import "./App.scss";
import { Input } from "./components/Input";
import { Button } from "./components/Button";
import { Checkbox } from "./components/Checkbox";

export const App = () => {
	return (
		<div className="main">
			<div className="container">
				<h1 className="title">Form</h1>

				<div className="container__body">
					<Input label="Login" inputType="text" />
					<Input label="Password" inputType="password" wrapperClassName="mt_20" />
					<Input label="Confirm password" inputType="password" wrapperClassName="mt_20" />

					<Checkbox className="" label="" isChecked={true} onValueChange={() => {}} />

					<Button
						htmlType="button"
						className="mt_40 mr_0 d_block"
						variant="primary"
						text="Next"
					/>
				</div>
			</div>
		</div>
	);
};
